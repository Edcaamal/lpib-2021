/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1b2021;

/**
 *
 * @author edgar
 */
public class Ciclos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("For secuencial +1  ");
        // 
        for (int i = 10; i <= 20; i++) {
            System.out.println(i);
        }

        System.out.println("For incrementando 2, en 2  ");
        // For incrementando 2, en 2
        for (int i = 0; i <=10 ; i+=2) {
            System.out.println(i);
        }

        System.out.println("For incrementando 3, en 3  ");
        for (int i = 0; i <=20 ; i+=3) {
            System.out.println(i);
        }

        System.out.println("For decrementando  ");
        // For Decremental
        for (int i = 10; i >= 0; i--) {
            System.out.println(i);
        }
        System.out.println("For decrementando 2, en 2  ");
        for (int i = 10; i >= 0; i-=2) {
            System.out.println(i);
        }

        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
            for (int j = 10; i <= 20; i++) {
                System.out.println(i*j);
            }
        }
        

    }
    
}
