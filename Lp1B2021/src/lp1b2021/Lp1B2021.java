/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1b2021;

/**
 *
 * @author edgar
 */
public class Lp1B2021 {
    public static     boolean linea;
    public static     String sMensaje = "1.- Hola Grupo A de Lenguajes de Programación I ";
    public static     String sMsgElse = "2.- La programación es lo máximo ... ";
    public static     int valor1 = 2;
    public static     int valor2 = 2;

    public static void encabezado() {
        System.out.println("Universida Autónoma de Campeche");
        System.out.println("Facultad de Ingeniería");
        System.out.println("Ingeniería de Sistemas Computacionales");
        separador();
    }
    public static void separador() {
        System.out.println("----------------------------------------------------");
    }

    public static void miMenu() {
        System.out.println("1.- If Booleano");
        System.out.println("2.- If Mayor Que");
        System.out.println("3.- If Igualdad");
        System.out.println("4.- If Anidado");
        System.out.println("5.- If Anidado Implicito");
        System.out.println("6.- Suma 2 Numeros ");
    }
        
    public static void piePagina() {
        separador();
        System.out.println("c) EDCD 2021");
    } 
    
    public static void ifBooleano() {
        // Primera Condición IF
        linea = false;
        System.out.println("-----------[Primer Ejemplo]-{Valor Booleano}--------------------------");
        System.out.println("ifBooleano");
        System.out.println(sMensaje);
        
        if (linea) {
           System.out.println(sMensaje);      
        }else{
           System.out.println(sMsgElse);
        }     
    }
    
    public static void ifMayorQue() {
       // crrear dos variables, una booleana que evaluarà el IF, y una de tipo string para imprimir un mensaje
       System.out.println("-----------[Segundo Ejemplo]-{Mayor Que}--------------------------");
       
       if (valor1 > valor2) {
          System.out.println(sMensaje);
       } else {
           System.out.println(sMsgElse);
       }
    }

    public static void ifIgualdad() {
       System.out.println("-----------[Tercer Ejemplo]-{Igualdad}--------------------------");
       if (valor1 == valor2) {
          System.out.println(sMensaje);
       }
    }
    
    public static void ifAnidado() {
       System.out.println("-----------[Cuarto Ejemplo]-{If Anidado Mayor Que}--------------------------");
       valor1 = 3;
       valor2 = 3;
       
       if (valor1 > valor2) {
          System.out.println(sMensaje);
       } else {
           if (valor1 < valor2){
               System.out.println(sMsgElse);
           }else{
               System.out.println("3.- Numeros Iguales");
           }
       }        
    }

    public static void ifAnidadoImplicito() {
       System.out.println("-----------[Quinto Ejemplo]-{If Mayor Que}--------------------------");
       valor1 = 3;
       valor2 = 1;
       
       if (valor1 > valor2) {
          System.out.println(sMensaje);
       } else if (valor1 < valor2){
            System.out.println(sMsgElse);
       }else{
            System.out.println("3.- Numeros Iguales");
       }            
    }
    
    public static int fSuma2numeros(int a, int b) {
       int valorregresa = a+b;
       return valorregresa;
    }
    
    public static void diaDeLaSemana(int dia, boolean tipoSemana) {
       if (tipoSemana){  // Seman Inglesa
            switch(dia)
             {
             case 1 :
                 System.out.println("Domingo");
                 break; 
             case 2 :
                 System.out.println("Lunes");
                 break; 
             case 3 :
                 System.out.println("Martes");
                 break; 
             case 4 :
                 System.out.println("Miercoles");
                 break; 
             case 5 :
                 System.out.println("Jueves");
                 break; 
             case 6 :
                 System.out.println("Viernes");
                 break; 
             case 7 :
                 System.out.println("Sabado");
                 break; 
             default : 
                 System.out.println("Opcion No válida");
             }
           
       } else {
        
            //Semana Laborable
           switch(dia)
            {
            case 1 :
                System.out.println("Lunes");
                break; 
            case 2 :
                System.out.println("Martes");
                break; 
            case 3 :
                System.out.println("Miercoles");
                break; 
            case 4 :
                System.out.println("Jueves");
                break; 
            case 5 :
                System.out.println("Viernes");
                break; 
            case 6 :
                System.out.println("Sabado");
                break; 
            case 7 :
                System.out.println("Domingo");
                break; 
            default : 
                System.out.println("Opcion No válida");
            }
       }
        
    }
    
    
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        encabezado();
        miMenu();
        
        // Leer un valor entero para la opcion deseada
       int opcion = 7;
       switch(opcion)
        {
        case 1 :
           ifBooleano();
            break; 
        case 2 :
           ifMayorQue();
            break; 
        case 3 :
            ifIgualdad();
            break; 
        case 4 :
            ifAnidado();
            break; 
        case 5 :
            ifAnidadoImplicito();
            break; 
        case 6 :
            System.out.println(fSuma2numeros(2,3));
            break; 
        case 7 :
            diaDeLaSemana(1, false);
            break; 
        default : 
            System.out.println("Opcion No válida");
        }

        piePagina();
       
    }
   
}
