/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1b2021;

import java.util.Scanner;

/**
 *
 * @author edgar
 */
public class ciclosWhile {

    public static void metodoDoWhile(){
         // Ciclico incremental de 1 en 1
        int x = 1;
        do {
            System.out.println("do While, Valor de x: " + x);
            x++;
        } while (x <= 10);

         // Ciclico incremental de 2 en 2
        x = 1;
        do {
            System.out.println("do While, Valor de x: " + x);
            x+=2;
        } while (x <= 10);

         // Ciclico incremental de 3 en 3
        x = 1;
        do {
            System.out.println("do While, Valor de x: " + x);
            x+=3;
        } while (x <= 10);

         // Ciclico decremental de 1 en 1
        x = 10;
        do {
            System.out.println("do While, Valor de x: " + x);
            x--;
        } while (x >= 1);        
    }

    public static void metodoWhile(){
        // Ciclico incremental de 1 en 1
        int x = 1;
        while (x <= 10){
            System.out.println("While Valor de x: " + x);
            x++;
        }
        
        // Ciclico incremental de 2 en 2
        x = 1;
        while (x <= 10){
            System.out.println("Valor de x: " + x);
            x+=2;
        }

        // Ciclico incremental de 3 en 3
        x = 1;
        while (x <= 10){
            System.out.println("Valor de x: " + x);
            x+=3;
        }

        // Ciclico decremental de 1 en 1
        x = 10;
        while (x >= 1){
            System.out.println("Valor decremental x: " + x);
            x--;
        }
    }
    
    public static void imprimirMensaje(String msg){
        System.out.println(msg);
    }
    
    
    public static void main(String[] args) {
        
        //ciclosWhile.metodoDoWhile();
        //ciclosWhile.metodoWhile();
        
        // metodoDoWhile();
        // 1metodoWhile();
        
       
        int opcionmenu; 
        do {
           imprimirMensaje("1.-Método While ");
           imprimirMensaje("2.-Método Do While ");
           imprimirMensaje("9.- Salir");           

           Scanner entrada = new Scanner( System.in );
           imprimirMensaje("Teccle la opción deseada __ ");        
           opcionmenu = entrada.nextInt();
           
            switch (opcionmenu) {
                case 1:
                    metodoWhile();
                    break;
                case 2:
                    metodoDoWhile();
                    break;
                case 9:
                    
                    break;
                default:
                    System.out.println("No es una opcion valida");    
            }
          //submenu(opcionmenu);
       } while (opcionmenu != 9);

        
    }
    
}
